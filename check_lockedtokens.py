#!/usr/bin/env python
import gridtools
import json
import os
import time
import argparse
import shutil
import sys


class bc:

    OK = '\033[92m'
    END = '\033[0m'


class Unlocker:
    maxScrubs = 0
    checked = 0
    db = None
    tokensToScrub = []

    def __init__(self, db, scrubs):
        self.db = db
        self.maxScrubs = scrubs

    def setMaxScrubs(self, scrubs):
        self.maxScrubs = scrubs

    def checkToken(self, doc):
        self.checked = self.checked + 1

        if "scrub_count" in doc:
            scrubs = doc["scrub_count"]
        else:
            scrubs = 0
        if scrubs < self.maxScrubs:
            gridtools.reset_doc_values(doc)
            self.tokensToScrub.append(doc)
            return True
        return False

    def unlock(self):
        cleantokens = []
        for doc in self.tokensToScrub:
            cleantokens.append(doc)
        if len(cleantokens) > 0:
            self.db.update(cleantokens)


parser = argparse.ArgumentParser(
    description='Check status of locked couchdb tokens.')


parser.add_argument('--max', dest="maxscrubs", type=int, default=0,
                    help="Set limit to amounts of scrubs to reset token ")

parser.add_argument('--reseterror', dest="cleanerror",action='store_true',
                    help="reset tokens in error state")

parser.add_argument('--reset-no-wms', dest="cleannowms",action='store_true',
                    help="reset all tokens that have no WMS id")



parser.set_defaults(cleanerror=False)


parse_results = parser.parse_args()

db = gridtools.connect_to_couchdb()
unlocker = Unlocker(db, parse_results.maxscrubs)
unlocker_no_wms = Unlocker(db, sys.maxsize)
to_inspect = []
for row in db.iterview(gridtools.credentials.VIEW_NAME + "/locked", 100):
    doc = row["value"]
    if "wms_job_id" in doc:
        if doc["wms_job_id"].startswith("http"):
            to_inspect.append(doc)
        else:
            print(doc["_id"] + " has no valid wmsid")
            unlocker_no_wms.checkToken(doc)
    else:
        print(doc["_id"] + "has no wms-id")



for doc in to_inspect:
    cmd = "glite-wms-job-status -json " + doc["wms_job_id"]
    report = gridtools.execute(cmd.split())
    status = json.loads(report["stdout"])
    statusclean = [status[k] for k in status.keys() if k.startswith("http")][0]
    hints = ""
    if statusclean["Current Status"] == "Running":
        runtime = (time.time() - doc["lock"]) / 3600.0
        hints = "\truntime:" + str(round(runtime, 2))

    if statusclean["Current Status"] == "Aborted":
        hints = "\t" + statusclean["Destination"] + \
            "\t" + statusclean["Status Reason"] + "\t" + \
            str(statusclean["Logged Reason"]) + "\truntime:" + \
            str((int(statusclean["Aborted"]) - doc["lock"]) / 3600.0)
        gridtools.reset_doc_values(doc)
        unlocker.checkToken(doc)
    if statusclean["Current Status"] == "Done(Exit Code !=0)":
        # retrieve logs from wms
        cmd = "glite-wms-job-output --json --nopurge " + doc["wms_job_id"]
        report = gridtools.execute(cmd.split())
        statusoutput = json.loads(report["stdout"])["jobs"]
        location = [statusoutput[k] for k in statusoutput.keys() if
                    k.startswith("http")][0]["location"]
        if os.path.exists(doc["_id"]):
            shutil.rmtree(doc["_id"])
        shutil.move(location, doc["_id"])

        # dump python logs

        fh = open(doc["_id"] + "/overview.log", "w")
        json.dump(doc, fh, sort_keys=True, indent=4, separators=(',', ': '))
        fh.close()

        # dump atahments
        if "_attachments" in doc:
            for attname in doc["_attachments"].keys():
                b = db.get_attachment(doc["_id"], attname)
                q = eval(b.read())
                fh = open(doc["_id"] + "/" + attname + ".log", "w")
                for k, v in q.iteritems():
                    fh.write(k + "\n")
                    fh.write(str(v) + "\n")
                fh.close()

        hints = "\t dump of logs made in working directory"
        if unlocker.checkToken(doc):
            hints += "\t" + bc.OK + "unlocked" + bc.END

    # pprint.pprint(statusclean)
    print(doc["_id"] + "\t " + statusclean["Current Status"] + hints)

unlocker.unlock()

if parse_results.cleannowms:
    unlocker_no_wms.unlock()


# handling tokens in error state

maxscrubs=0
if (parse_results.cleanerror):
    maxscrubs=sys.maxsize
unlocker_err = Unlocker(db, maxscrubs)

for row in db.iterview(gridtools.credentials.VIEW_NAME + "/error", 100):
    doc = row["value"]
    print ( "{} token is in error state\n\n{}\n{}\n".format(doc["_id"],doc["error"][-1]["message"],doc["error"][-1]["exception"]) )
    unlocker_err.checkToken(doc)

unlocker_err.unlock()


