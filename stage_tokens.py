#!/usr/bin/env python
# -*- coding: utf-8 -*-
##
# Example : bring online a set of files with gfal2
#
import errno
import gfal2
import time
import re
import gridtools
import credentials
import logging


class Experiment:

    def __init__(self, tokenname):

        self.tokenname = tokenname
        self.stage_timeout = int(3600 * 72)
        self.pin_time = int(3600 * 72)
        self.stagetoken = ""
        self.filesoffline = []

        db_worker = gridtools.connect_to_couchdb()

        doc = db_worker[self.tokenname]
        doc["stage_lock"] = int(time.time())
        doc_id, doc_rev = db_worker.save(doc)
        doc["_rev"] = doc_rev
        self.doc = doc

        m = re.compile('.*/pnfs')

        turls = []
        for filetype in doc["files"]:
            turls.append(str(doc["files"][filetype]["url"]))
        self.surls = [
            m.sub(
                'srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs',
                k) for k in turls]

        self.filesoffline = list(self.surls)

    def evaluate_errors(self, errors, surls, polling):
        n_terminal = 0
        for surl, error in zip(surls, errors):
            surl = surl.replace(
                "srm://srm.grid.sara.nl:8443/srm/managerv2?SFN=/pnfs/grid.sara.nl/data/", "")
            if error:
                if error.code != errno.EAGAIN:
                    logging.error("%s => FAILED: %s" % (surl, error.message))
                    logging.error("bringing  files again online")

                    self.bringOnline()
                    n_terminal += 1
                else:
                    logging.info("%s QUEUED" % surl)
                    pass
            elif not polling:
                logging.info("%s QUEUED" % surl)
            else:
                n_terminal += 1
                # print "%s READY" % surl
        return n_terminal

    def bringOnline(self):
        """
        """
        self.RemoveOnlineFiles()
        if (len(self.filesoffline) != 0):
            logging.debug("bringing online " + str(self.tokenname))
            ctx = gfal2.creat_context()
            (errors, token) = ctx.bring_online(
                self.filesoffline, self.pin_time, self.stage_timeout, True)
            self.stagetoken = token

            errors = ctx.bring_online_poll(
                self.filesoffline, self.stagetoken)
        else:
            self.setStaged()

    def RemoveOnlineFiles(self):
        """
        """

        ctx = gfal2.creat_context()
        for surl in self.filesoffline:
            logging.debug("checking file" + str(surl))

            status = ctx.getxattr(surl, "user.status")

            # print(surl+status)
            if status == "ONLINE_AND_NEARLINE":
                self.filesoffline.remove(surl)
                logging.debug("file online" + str(surl))
        logging.debug("offlinefiles:" + str(self.filesoffline))

    def checkOnline(self):
        """

        """
        self.RemoveOnlineFiles()

        if (len(self.filesoffline) == 0):

            return(True)
        else:
            ctx = gfal2.creat_context()
            errors = ctx.bring_online_poll(self.filesoffline, self.stagetoken)
            n_terminal = self.evaluate_errors(
                errors, self.filesoffline, polling=True)

            return(False)

    def ready(self):
        if (self.doc["stage_done"] > 0):

            return(True)
        else:
            return(False)

    def setStaged(self):
        """

        """
        db = gridtools.connect_to_couchdb()
        self.doc["stage_done"] = int(time.time())

        try:
            db[self.tokenname] = self.doc
        except:
            logging.warning("saving doc goes wrong. Probally staged in another way")

        # move file from wait to stage to staging
        gridtools.wait2stage_to_stage(1)



if __name__ == '__main__':

    logger = logging.getLogger()
    handler = logging.StreamHandler()
    formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    logger.setLevel(logging.INFO)

    logging.getLogger("gfal2").setLevel(logging.WARNING)

    # Check all unstaged tokens
    overview=gridtools.check_unstaged_tokens()
    retryinterval = 600
    db = gridtools.connect_to_couchdb()

    total_tostage = gridtools.docs_in_view("to_stage")
    logging.info("new jobs found:" + str(total_tostage))

    max_to_stage=750-gridtools.docs_in_view("staging")

    gridtools.wait2stage_to_stage( max(max_to_stage-total_tostage,0))
    
    # get all tokens todo
    jobs = []
    for row in db.iterview(credentials.VIEW_NAME + "/staging", 100):
        stagejob = Experiment(row["key"])
        stagejob.bringOnline()
        jobs.append(stagejob)

    while True:
        starttime = int(time.time())

        # adding new tokens
        for row in db.iterview(credentials.VIEW_NAME + "/to_stage", 100):
            stagejob = Experiment(row["key"])
            stagejob.bringOnline()
            jobs.append(stagejob)
        

        #stop staging if no jobs are left
        if len(jobs)==0:
            logging.info("No stagging requests left.")
            break

        jobs_done=[]
        for n in range(0,len(jobs)):
            job=jobs[n]
            # print(job.tokenname)
            if not job.ready():
                if job.checkOnline():
                    logging.debug("staging done " + job.tokenname)
                    job.setStaged()
                    jobs_done.append(n)

        #remove jobs that are done
        for index in sorted(jobs_done, reverse=True):
            del jobs[index]

        logging.info("staged {} tokens, {} tokens are queued".format(len(jobs_done),len(jobs))) 

        sleeptime = retryinterval - (int(time.time()) - starttime)
        # print(sleeptime)
        logging.info("sleep until next online check {} seconds".format(max(0, sleeptime)))
        #sleep interval of 1 sec makes it able to exit the script with control+c after one sec instead of 600 sec
        for i in range(max(0, sleeptime)):
            time.sleep(1)
        