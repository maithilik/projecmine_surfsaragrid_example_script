#!/usr/bin/env bash
 
set +e
set -o pipefail
#set -x

wait=${1:-300} 
jobs=${2:-25} 

while [[ true ]]; do

sleep $wait&
voms-proxy-info --fqan |head -n 1 | grep Project_MinE >/dev/null
if [ $? -ne 0 ]; then
        echo 'proxy does has not right VOMS: Please run startGridSession lsgrid:/lsgrid/ProjectMinE '
        exit 1
fi

voms-proxy-info -timeleft >/dev/null
if [ $? -ne 0 ]; then
	echo 'proxy not valid any more: Please run startGridSession lsgrid:/lsgrid/ProjectMinE '
	exit 1
fi

myproxy-info -s px.grid.sara.nl -d >/dev/null
if [ $? -ne 0 ]; then
        echo 'proxy not valid or not found on proxy server: Please run startGridSession lsgrid:/lsgrid/ProjectMinE '
        exit 1
fi

total_jobs=`python  -c "import gridtools;import logging;logging.basicConfig(level=logging.DEBUG);print(gridtools.totaljobs_to_proccess())"`
if [ $total_jobs -eq 0 ]; then
	echo 'All jobs processed'
	exit 0
fi

python  -c "import gridtools;import logging;logging.basicConfig(level=logging.DEBUG);gridtools.autosubmit('gridjob.jdl',$jobs)"

wait
done
