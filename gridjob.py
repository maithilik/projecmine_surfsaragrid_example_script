#!/usr/bin/env python
# encoding: utf-8

import gridtools
import credentials
import logging
import os

'''

Gridjob -- shortdesc

Boiler plate code to run Project Mine Jobs.

@author:     Maarten Kooyman and Joke van Vugt

@copyright:  2015-2017 SURFsara. All rights reserved.

@license:    Apache 2.0

@contact:    projectmine@SURFsara.nl or j.f.a.vanvugt-2@umcutrecht.nl

'''

TMPDIR = os.environ['TMPDIR'] + "/"

SOFTDIR = "/cvmfs/softdrive.nl/maartenk/project_mine/"
REFGENOME = SOFTDIR + "reference/ucsc.hg19_fixed_maarten.fasta"
MILLS = SOFTDIR + "reference/Mills_and_1000G_gold_standard.indels.hg19_no_info.recode.vcf.gz"
THOUSANDG = SOFTDIR + "reference/1000G_phase1.indels.hg19_no_info.recode.vcf.gz"
DBSNP = SOFTDIR + "reference/dbsnp142_noinfo_chr_prefix.vcf.gz"

REMOTE_BASE_URL = "srm://srm.grid.sara.nl/pnfs/grid.sara.nl/data/lsgrid/Project_MinE/"

_log_fmt = '%(asctime)s\t%(levelname)s\t%(lineno)d\t%(message)s'
_log_date_fmt = '%Y/%m/%d %H:%M:%S'
logger = logging.getLogger('mylogger')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter(fmt=_log_fmt, datefmt=_log_date_fmt)
ch = logging.StreamHandler()
ch.setFormatter(formatter)
logger.addHandler(ch)

class Bamlet(gridtools.MineRunner):

    def worker(self, token):

            filemapping = self.download_all_files(token["files"])

            convert_cram_to_bam=False

            # Define bam files, cram decompression needed
            if convert_cram_to_bam and token["format"]=="cram":
                # decompress cram to bam
                (bam_path, bam_index_path) = self.decompress_cram(filemapping["cram"], token["sampleid"])

                # remove cram file from local disk to keep disk clean
                os.remove(filemapping["cram"])
                os.remove(filemapping["crai"])

            # Define bam files, no cram decompression needed
            elif (not convert_cram_to_bam and token["format"]=="cram"):
                bam_path = filemapping["cram"]
                bam_index_path=filemapping["crai"]

            else:
                bam_index_path=filemapping["bai"]
                bam_path=filemapping["bam"]

            cmd = """

    # extract UNC13A gene from bam file
    samtools view -h $bam_file chr19:17712137-17799050 | samtools view -bS - > $sample"_UNC13A.bam"

    # generate index from new bam file
    samtools index $sample"_UNC13A.bam"

            """

            # define variables from the bash sniplet
            bam_file = os.path.basename(bam_path)
            sample = token["sampleid"]

            # substitute variables for the actual values
            cmd = gridtools.templater(cmd)

            # run script as a bash script
            self.execute(gridtools.run_as_bash(cmd))

            # zip results
            results_path = sample + ".tar.gz"
            cmd = [ "tar", "-czvf", results_path, sample + "_UNC13A.bam", sample + "_UNC13A.bam.bai" ]
            job_report = self.execute(cmd)

            # upload results
            remote_results = REMOTE_BASE_URL + "Joke/ExampleBamlet/" + sample + ".tar.gz"
            gridtools.upload_file(results_path, remote_results)

    def decompress_cram(self, cram_path, jobname):
        """
        """
        bam_path = os.getcwd() + "/" + jobname + ".bam"
        bam_index_path = bam_path + ".bai"
        logger.debug("starting decompression")

        cmd = [ "samtools", "view", "-h", "-b", "-1", "-T", "/cvmfs/softdrive.nl/maartenk/project_mine/reference/ucsc.hg19_fixed_maarten.fasta", "-o", bam_path, cram_path ]
        job_report = self.execute(cmd)

        cmd = ["samtools", "index", bam_path]
        job_report = self.execute(cmd)
        return(bam_path, bam_index_path)

def main():

    client = gridtools.CouchDB(url=credentials.URL, username=credentials.USERNAME, password=credentials.PASS, db=credentials.DBNAME)
    iterator = gridtools.TaskViewIterator(client, "todo",design_doc=credentials.VIEW_NAME)
    modifier = gridtools.BasicTokenModifier()
    actor = Bamlet(iterator, modifier)

    # set maximum time to process task in current job
    max_time = int(24 * 3600)

    # start processing 
    actor.run(maxtime=max_time)

if __name__ == '__main__':

    main()
