#!/bin/bash

set -x
set -e

export PATH=/cvmfs/softdrive.nl/$USER/Miniconda2/bin:$PATH
mail="yourname@something.com"

#unset java options
unset _JAVA_OPTIONS

freespace=`stat --format "%a*%s/1024^3" -f $TMPDIR|bc`
echo $freespace

df $TMPDIR
if [ $freespace -gt 150 ]; then  echo "enough free space in scratch: $freespace GB" ; else echo "not enough free space in scratch: $freespace GB"|  mail -s "Not enough diskspace found at $HOSTNAME" $mail; exit 1; fi

# test if softdrive is present
if [ -d /cvmfs/softdrive.nl/maartenk/project_mine ]
  then
    echo "Softdrive found"
 else
        echo "softdrive not found"
        echo "There is no softdrive found on $HOSTNAME" | mail -s "No softdrive found at $HOSTNAME" $mail
        exit 1
fi

export REF_PATH=/cvmfs/softdrive.nl/maartenk/project_mine/reference/cache/%2s/%2s/%s:http://www.ebi.ac.uk/ena/cram/md5/%s
export REF_CACHE=/cvmfs/softdrive.nl/maartenk/project_mine/reference/cache/%2s/%2s/%s

python gridjob.py
