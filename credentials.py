# Replace 'yourname' with your CouchDB username
# Replace 'TopSecret' with your CouchDB password
# Replace 'project_mine_yourname' with your CouchDB folder name
# Replace 'Bamlets_example' with a name that describes your workflow
USERNAME = "yourname"
PASS = "TopSecret"
URL = "https://nosql01.grid.sara.nl:6984/"
DBNAME = "project_mine_yourname"
VIEW_NAME = "Bamlets_example"
